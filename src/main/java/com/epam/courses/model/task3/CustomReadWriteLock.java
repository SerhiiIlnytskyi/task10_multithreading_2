package com.epam.courses.model.task3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CustomReadWriteLock {

  private static Logger log = LogManager.getLogger(CustomReadWriteLock.class);

  private int readLocks = 0;
  private int writeLocks = 0;
  private int writeLockRequests = 0;

  public synchronized void lockRead() throws InterruptedException {
    while (writeLocks > 0 || writeLockRequests > 0) {
      log.info("Locked for write. Waiting for read possibility...");
      wait();
    }
    readLocks++;
  }

  public synchronized void unlockRead() {
    readLocks--;
    notifyAll();
  }

  public synchronized void lockWrite() throws InterruptedException {
    writeLockRequests++;
    while (readLocks > 0 || writeLocks > 0) {
      log.info("Locked for write or read. Waiting for write possibility ...");
      wait();
    }
    writeLockRequests--;
    writeLocks++;
  }

  public synchronized void unlockWrite() {
    writeLocks--;
    notifyAll();
  }

}
