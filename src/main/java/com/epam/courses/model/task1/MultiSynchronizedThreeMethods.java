package com.epam.courses.model.task1;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MultiSynchronizedThreeMethods {

  private static Logger log = LogManager.getLogger(MultiSynchronizedThreeMethods.class);
  private final static Lock firstLock = new ReentrantLock();
  private final static Lock secondLock = new ReentrantLock();
  private final static Lock thirdLock = new ReentrantLock();

  public void firstMethod() {
    firstLock.lock();
    try {
      for (int i = 0; i < 15; i++) {
        log.info("Another lock object: first Method message");
        Thread.yield();
      }
    } finally {
      firstLock.unlock();
    }
  }

  public void secondMethod() {
    secondLock.lock();
    try {
      for (int i = 0; i < 15; i++) {
        log.info("Another lock object: second Method message");
        Thread.yield();
      }
    } finally {
      secondLock.unlock();
    }
  }

  public void thirdMethod() {
    thirdLock.lock();
    try {
      for (int i = 0; i < 15; i++) {
        log.info("Another lock object: third Method message");
        Thread.yield();
      }
    } finally {
      thirdLock.unlock();
    }
  }
}
