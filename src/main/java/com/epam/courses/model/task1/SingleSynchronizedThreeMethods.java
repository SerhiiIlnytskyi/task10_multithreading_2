package com.epam.courses.model.task1;

import java.util.concurrent.locks.ReentrantLock;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SingleSynchronizedThreeMethods {

  private static Logger log = LogManager.getLogger(SingleSynchronizedThreeMethods.class);
  private final static ReentrantLock lock = new ReentrantLock();

  public void firstMethod() {
    lock.lock();
    try {
      for (int i = 0; i < 15; i++) {
        log.info("Same lock object: first Method message");
        Thread.yield();
      }
    } finally {
      lock.unlock();
    }
  }

  public void secondMethod() {
    lock.lock();
    try {
      for (int i = 0; i < 15; i++) {
        log.info("Same lock object: second Method message");
        Thread.yield();
      }
    } finally {
      lock.unlock();
    }
  }

  public void thirdMethod() {
    lock.lock();
    try {
      for (int i = 0; i < 15; i++) {
        log.info("Same lock object: third Method message");
        Thread.yield();
      }
    } finally {
      lock.unlock();
    }
  }
}
