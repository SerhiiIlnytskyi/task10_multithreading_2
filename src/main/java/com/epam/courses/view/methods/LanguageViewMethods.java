package com.epam.courses.view.methods;

import com.epam.courses.controller.MainViewController;
import com.epam.courses.view.View;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

public class LanguageViewMethods implements ViewMethods {

  private Map<String, Executable> methodsMenu;

  public LanguageViewMethods() {
    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::buttonUaMenu);
    methodsMenu.put("2", this::buttonEnMenu);
  }

  private void buttonEnMenu() {
    new View("MainView", new Locale("en")).show();
  }

  private void buttonUaMenu() {
    new View("MainView", new Locale("uk")).show();
  }

  public Map<String, Executable> getMethodsMenu() {
    return methodsMenu;
  }

  public void setMethodsMenu(Map<String, Executable> methodsMenu) {
    this.methodsMenu = methodsMenu;
  }

}
