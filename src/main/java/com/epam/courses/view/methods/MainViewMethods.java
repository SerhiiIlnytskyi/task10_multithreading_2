package com.epam.courses.view.methods;

import com.epam.courses.controller.MainViewController;
import com.epam.courses.view.View;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MainViewMethods implements ViewMethods{

  private static Logger log = LogManager.getLogger(MainViewMethods.class);

  private MainViewController mainViewController;
  private Map<String, Executable> methodsMenu;

  public MainViewMethods() {
    this.mainViewController = new MainViewController();
    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::button1);
    methodsMenu.put("2", this::button2);
    methodsMenu.put("3", this::button3);
    methodsMenu.put("L", this::languageMenu);
  }
  private void button1() {
    log.info("First task tests started....");
    mainViewController.runFirstTaskTest();
    log.info("First task test completed.");
  }

  private void button2() {
    log.info("Second task tests started....");
    mainViewController.runSecondTaskTest();
    log.info("Second task test completed.");
  }

  private void button3() {
    log.info("Third task tests started....");
    mainViewController.runThirdTaskTest();
    log.info("Third task test completed.");
  }

  private void languageMenu() {
    new View("LanguageView").show();
  }

  public Map<String, Executable> getMethodsMenu() {
    return methodsMenu;
  }

  public void setMethodsMenu(Map<String, Executable> methodsMenu) {
    this.methodsMenu = methodsMenu;
  }

}
