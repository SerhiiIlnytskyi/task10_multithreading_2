package com.epam.courses.controller;

import com.epam.courses.model.task1.MultiSynchronizedThreeMethods;
import com.epam.courses.model.task1.SingleSynchronizedThreeMethods;
import com.epam.courses.model.task2.Consumer;
import com.epam.courses.model.task2.Producer;
import com.epam.courses.model.task3.CustomReadWriteLock;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MainViewController {

  private static Logger log = LogManager.getLogger(MainViewController.class);
  private static Random random = new Random();

  public void runFirstTaskTest() {
    MultiSynchronizedThreeMethods threeMethods = new MultiSynchronizedThreeMethods();
    new Thread(threeMethods::firstMethod).start();
    new Thread(threeMethods::secondMethod).start();
    new Thread(threeMethods::thirdMethod).start();

    SingleSynchronizedThreeMethods singleSynchronizedThreeMethods = new SingleSynchronizedThreeMethods();
    Thread testThread1 = new Thread(singleSynchronizedThreeMethods::firstMethod);
    testThread1.start();
    Thread testThread2 = new Thread(singleSynchronizedThreeMethods::secondMethod);
    testThread2.start();
    Thread testThread3 = new Thread(singleSynchronizedThreeMethods::thirdMethod);
    testThread3.start();
    try {
      testThread1.join();
      testThread2.join();
      testThread3.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void runSecondTaskTest() {
    Producer producer = new Producer();
    LinkedBlockingQueue<Character> characterLinkedBlockingQueue = producer.getBlockingQueue();

    Consumer consumer = new Consumer(characterLinkedBlockingQueue);

    Thread thread1 = new Thread(producer);
    Thread thread2 = new Thread(consumer);

    thread1.start();
    thread2.start();

    try {
      thread1.join();
      thread2.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void runThirdTaskTest() {
    CustomReadWriteLock customReadWriteLock = new CustomReadWriteLock();
    BlockingQueue<Integer> queue = new LinkedBlockingQueue<>();

    Thread threadWrite = new Thread(() -> {
      for (int i = 0; i < 20; i++) {
        try {
          customReadWriteLock.lockWrite();
          Integer value = random.nextInt(100);
          log.info(String.format("Generated value %d. Tried to write it.", value));
          queue.put(value);
        } catch (InterruptedException e) {
          e.printStackTrace();
        } finally {
          customReadWriteLock.unlockWrite();
        }
      }
    });

    Thread threadRead = new Thread(() -> {
      for (int i = 0; i < 20; i++) {
        try {
          customReadWriteLock.lockRead();
          log.info("Readed: " + queue.poll());
        } catch (InterruptedException e) {
          e.printStackTrace();
        } finally {
          customReadWriteLock.unlockRead();
        }
      }
    });

    threadWrite.start();
    threadRead.start();

    try {
      threadWrite.join();
      threadRead.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

  }
}
